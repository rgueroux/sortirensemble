<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sorties extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sorties';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'idSortie';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['nom','datedebut','duree','datecloture','nbinscriptionsmax','descriptioninfos','etatsortie','urlPhoto','organisateur','lieux_idLieu','etats_idEtat'];
}
