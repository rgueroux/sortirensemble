<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class lieux extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'lieux';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'idLieu';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['nom_lieu','rue','latitude','longitude','villes_idVille'];
}
