<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class inscriptions extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'inscriptions';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['date_inscription','sorties_idSortie','participants_idParticipant'];
}
