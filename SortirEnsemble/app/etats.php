<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class etats extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'etats';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'idEtat';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['libelle'];
}
