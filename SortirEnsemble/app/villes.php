<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class villes extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'villes';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'idVille';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['nom_ville','code_postal'];
}
